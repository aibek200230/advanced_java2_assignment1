package com.company;

import java.util.Date;

public class Article {
    private int id;
    private int source_id;
    private String source_name;
    private String title;
    private String content;
    private Date published_at;

    Article(){}

    public Article(int id, int source_id, String source_name, String title, String content, Date published_at) {
        this.id = id;
        this.source_id = source_id;
        this.source_name = source_name;
        this.title = title;
        this.content = content;
        this.published_at = published_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSource_id() {
        return source_id;
    }

    public void setSource_id(int source_id) {
        this.source_id = source_id;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPublished_at() {
        return published_at;
    }

    public void setPublished_at(Date published_at) {
        this.published_at = published_at;
    }

    public static Article createArticle(String[] article){
        int id = Integer.parseInt(article[0]);
        int source_id = Integer.parseInt(article[1]);
        String source_name = article[2];
        String title = article[3];
        String content = article[4];
        Date published_at = new Date(article[5]);

        return new Article(id, source_id, source_name, title, content, published_at);
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", source_id=" + source_id +
                ", source_name='" + source_name + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", published_at=" + published_at +
                '}';
    }
}
