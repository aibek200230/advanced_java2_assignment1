package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Article> articles = readArticlesFromCSV("C:\\Users\\aibek\\IdeaProjects\\ajp2_assignment1\\lists\\list1.txt");
        for (Article a: articles){
            System.out.println(a);
        }

    }

    private static List<Article> readArticlesFromCSV(String fileName){
        List<Article> articles = new ArrayList<>();
        Path pathToFile = Paths.get(fileName);
        try(BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {
            String line = br.readLine();
            while (line!=null){
                String[] attributes = line.split(",");
                Article article = Article.createArticle(attributes);
                articles.add(article);
                line = br.readLine();
            }
        } catch (IOException ioException){
            ioException.printStackTrace();
        }
        return articles;
    }
}
